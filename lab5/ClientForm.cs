﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace lab5
{
    public partial class ClientForm : Form
    {
        private readonly string sqlConnect = "Server=tcp:itacademy.database.windows.net,1433;Database=Kropyvnytskyi;User ID=Kropyvnytskyi;Password=Wboe73015;Trusted_Connection=False;Encrypt=True;";
        public ClientForm()
        {
            InitializeComponent();
        }

        private void ClientForm_Load(object sender, EventArgs e)
        {
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT Client.ID,Client.PIB, Client.contacts FROM [dbo].[Client]", connect);
            SqlDataReader dataReader = DA.SelectCommand.ExecuteReader();
            cmbClients.Items.Clear();
            while (dataReader.Read())
            {
                Client client = new Client();
                client.ID = (int)dataReader[0];
                client.Name = dataReader[1].ToString();
                client.Contacts = dataReader[2].ToString();
                cmbClients.Items.Add(client);
                cmbClients.DisplayMember = "Name";
            }
            connect.Close();
        }

        private void btnGetWorkersInfo_Click(object sender, EventArgs e)
        {
            Client client = (Client)cmbClients.SelectedItem;
            if (cmbClients.SelectedItem == null)
            {
                MessageBox.Show("You didnt choose anything, so the system will show you first value");
                client = (Client)cmbClients.Items[0];
            }
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT Client.ID,Client.PIB,Client.contacts FROM Client WHERE ID=" + client.ID.ToString(), connect);
            DataSet DS = new DataSet();
            DA.Fill(DS, "client_info");
            dgvInfo.DataSource = DS.Tables["client_info"];
            dgvInfo.Refresh();
            connect.Close();
        }

        private void btnSetClientInfo_Click(object sender, EventArgs e)
        {
            if (CheckInfo(PIB.Text, mContacts.Text))
            {
                SqlConnection connect = new SqlConnection(sqlConnect);
                connect.Open();
                SqlDataAdapter DA = new SqlDataAdapter($"INSERT INTO [dbo].[Client]([PIB] ,[contacts]) VALUES ('{PIB.Text}'  ,'{mContacts.Text}')", connect);
                DA.SelectCommand.ExecuteReader();
                MessageBox.Show("Client info has been added successfully");
                connect.Close();
            }
            else
            {
                MessageBox.Show("Error,some fields are empty, or contain invalid value");
            }

        }

        private void btnUpdateClientInfo_Click(object sender, EventArgs e)
        {
            if (cmbClients.SelectedItem != null)
            {
                Client client = (Client)cmbClients.SelectedItem;
                List<string> data = new List<string>();
                data.Add(client.Name);
                data.Add(client.Contacts);
                List<string> newData = new List<string>();
                newData.Add(PIB.Text);
                newData.Add(mContacts.Text);
                for (int i = 0; i < data.Count; i++)
                {
                    if (newData[i] != "")
                    {
                        data[i] = newData[i];
                    }
                }
                if (data[1] == "+38")
                {
                    data[1] = client.Contacts;
                }
                if (CheckInfo(data[0], data[1]))
                {
                    SqlConnection connect = new SqlConnection(sqlConnect);
                    connect.Open();
                    SqlDataAdapter DA = new SqlDataAdapter($"UPDATE [dbo].[Client] SET [PIB] = '{data[0]}',[contacts] = '{data[1]}'WHERE ID={client.ID}", connect);
                    DA.SelectCommand.ExecuteReader();
                    MessageBox.Show("Client info has been updated successfully");
                    connect.Close();
                }
                else
                {
                    MessageBox.Show("Error,some fields contain invalid value");
                }
            }
            else
            {
                MessageBox.Show("Error,you didnt choose a value from combobox.Please do so");
            }

        }
        private bool CheckInfo(string PIB, string contacts)
        {
            if (PIB == "" || contacts == "")
            {
                return false;
            }
            for (int i = 0; i < PIB.Length; i++)
            {
                if (char.IsNumber(PIB, i))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
