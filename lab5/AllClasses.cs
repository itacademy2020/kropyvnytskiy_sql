﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab5
{
    class Work
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    class Operator : Work
    {
        public string Contacts { get; set; }
        public string DateStart { get; set; }
        public string Address { get; set; }
    }
    class Client: Work
    {
        public string Contacts { get; set; }
    }
    class Hotline : Work
    {
        public string DateWork { get; set; }
        public string DateStart { get; set; }
        public string DateEnd { get; set; }
        public string TimeStart { get; set; }
        public string TimeEnd { get; set; }
    }
}
