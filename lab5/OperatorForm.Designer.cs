﻿namespace lab5
{
    partial class OperatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.mContacts = new System.Windows.Forms.MaskedTextBox();
            this.btnUpdateClientInfo = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.address = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.PIB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSetWorkerInfo = new System.Windows.Forms.Button();
            this.btnGetWorkersInfo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbWorkers = new System.Windows.Forms.ComboBox();
            this.dgvInfo = new System.Windows.Forms.DataGridView();
            this.mDateStart = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.mDateStart);
            this.splitContainer1.Panel1.Controls.Add(this.mContacts);
            this.splitContainer1.Panel1.Controls.Add(this.btnUpdateClientInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.address);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.PIB);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.btnSetWorkerInfo);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetWorkersInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbWorkers);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvInfo);
            this.splitContainer1.Size = new System.Drawing.Size(1140, 450);
            this.splitContainer1.SplitterDistance = 215;
            this.splitContainer1.TabIndex = 0;
            // 
            // mContacts
            // 
            this.mContacts.Location = new System.Drawing.Point(12, 206);
            this.mContacts.Mask = "+380000000000";
            this.mContacts.Name = "mContacts";
            this.mContacts.Size = new System.Drawing.Size(190, 22);
            this.mContacts.TabIndex = 1;
            // 
            // btnUpdateClientInfo
            // 
            this.btnUpdateClientInfo.Location = new System.Drawing.Point(93, 358);
            this.btnUpdateClientInfo.Name = "btnUpdateClientInfo";
            this.btnUpdateClientInfo.Size = new System.Drawing.Size(109, 23);
            this.btnUpdateClientInfo.TabIndex = 9;
            this.btnUpdateClientInfo.Text = "Update info";
            this.btnUpdateClientInfo.UseVisualStyleBackColor = true;
            this.btnUpdateClientInfo.Click += new System.EventHandler(this.btnUpdateInfo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 298);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 17);
            this.label5.TabIndex = 8;
            this.label5.Text = "address";
            // 
            // address
            // 
            this.address.Location = new System.Drawing.Point(12, 318);
            this.address.Name = "address";
            this.address.Size = new System.Drawing.Size(190, 22);
            this.address.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 246);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 17);
            this.label4.TabIndex = 6;
            this.label4.Text = "date start";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "contacts";
            // 
            // PIB
            // 
            this.PIB.Location = new System.Drawing.Point(12, 151);
            this.PIB.Name = "PIB";
            this.PIB.Size = new System.Drawing.Size(190, 22);
            this.PIB.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "PIB";
            // 
            // btnSetWorkerInfo
            // 
            this.btnSetWorkerInfo.Location = new System.Drawing.Point(12, 358);
            this.btnSetWorkerInfo.Name = "btnSetWorkerInfo";
            this.btnSetWorkerInfo.Size = new System.Drawing.Size(75, 23);
            this.btnSetWorkerInfo.TabIndex = 1;
            this.btnSetWorkerInfo.Text = "Set info";
            this.btnSetWorkerInfo.UseVisualStyleBackColor = true;
            this.btnSetWorkerInfo.Click += new System.EventHandler(this.btnSetWorkerInfo_Click);
            // 
            // btnGetWorkersInfo
            // 
            this.btnGetWorkersInfo.Location = new System.Drawing.Point(12, 71);
            this.btnGetWorkersInfo.Name = "btnGetWorkersInfo";
            this.btnGetWorkersInfo.Size = new System.Drawing.Size(75, 23);
            this.btnGetWorkersInfo.TabIndex = 0;
            this.btnGetWorkersInfo.Text = "Get info";
            this.btnGetWorkersInfo.UseVisualStyleBackColor = true;
            this.btnGetWorkersInfo.Click += new System.EventHandler(this.btnGetWorkersInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Operator";
            // 
            // cmbWorkers
            // 
            this.cmbWorkers.FormattingEnabled = true;
            this.cmbWorkers.Location = new System.Drawing.Point(12, 32);
            this.cmbWorkers.Name = "cmbWorkers";
            this.cmbWorkers.Size = new System.Drawing.Size(144, 24);
            this.cmbWorkers.TabIndex = 0;
            // 
            // dgvInfo
            // 
            this.dgvInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInfo.Location = new System.Drawing.Point(3, 0);
            this.dgvInfo.Name = "dgvInfo";
            this.dgvInfo.RowHeadersWidth = 51;
            this.dgvInfo.RowTemplate.Height = 24;
            this.dgvInfo.Size = new System.Drawing.Size(918, 450);
            this.dgvInfo.TabIndex = 0;
            // 
            // mDateStart
            // 
            this.mDateStart.AccessibleDescription = "";
            this.mDateStart.AccessibleName = "";
            this.mDateStart.Location = new System.Drawing.Point(15, 273);
            this.mDateStart.Mask = "0000-00-00";
            this.mDateStart.Name = "mDateStart";
            this.mDateStart.Size = new System.Drawing.Size(190, 22);
            this.mDateStart.TabIndex = 10;
            // 
            // OperatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1140, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "OperatorForm";
            this.Text = "OperatorForm";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnGetWorkersInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvInfo;
        private System.Windows.Forms.Button btnSetWorkerInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PIB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnUpdateClientInfo;
        private System.Windows.Forms.ComboBox cmbWorkers;
        private System.Windows.Forms.MaskedTextBox mContacts;
        private System.Windows.Forms.MaskedTextBox mDateStart;
    }
}

