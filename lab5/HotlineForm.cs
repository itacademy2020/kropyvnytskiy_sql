﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace lab5
{

    public partial class HotlineForm : Form
    {
        private readonly string sqlConnect = "Server=tcp:itacademy.database.windows.net,1433;Database=Kropyvnytskyi;User ID=Kropyvnytskyi;Password=Wboe73015;Trusted_Connection=False;Encrypt=True;";
        public HotlineForm()
        {
            InitializeComponent();
        }

        private void CallForm_Load(object sender, EventArgs e)
        {
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Hotline]", connect);
            SqlDataReader dataReader = DA.SelectCommand.ExecuteReader();
            cmbHotline.Items.Clear();
            while (dataReader.Read())
            {
                Hotline hotline = new Hotline();
                hotline.ID = (int)dataReader[0];
                hotline.Name = dataReader[1].ToString();
                hotline.DateWork = dataReader[2].ToString();
                hotline.DateStart = dataReader[3].ToString();
                hotline.DateEnd = dataReader[4].ToString();
                hotline.TimeStart = dataReader[5].ToString();
                hotline.TimeEnd = dataReader[6].ToString();
                cmbHotline.Items.Add(hotline);
                cmbHotline.DisplayMember = "Name";
            }
            connect.Close();
        }

        private void btnGetWorkersInfo_Click(object sender, EventArgs e)
        {
            Hotline hotline = (Hotline)cmbHotline.SelectedItem;
            if (cmbHotline.SelectedItem == null)
            {
                MessageBox.Show("You didnt choose anything, so the system will show you first value");
                hotline = (Hotline)cmbHotline.Items[0];
            }
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT * FROM [dbo].[Hotline] WHERE ID=" + hotline.ID.ToString(), connect);
            DataSet DS = new DataSet();
            DA.Fill(DS, "hotline_info");
            dgvInfo.DataSource = DS.Tables["hotline_info"];
            dgvInfo.Refresh();
            connect.Close();
        }

        private void btnSetWorkerInfo_Click(object sender, EventArgs e)
        {
            if (CheckInfo(name.Text, mDate_work.Text, date_start.Text, date_end.Text, mTime_start.Text, mTime_end.Text))
            {
                SqlConnection connect = new SqlConnection(sqlConnect);
                try
                {
                    connect.Open();
                    SqlDataAdapter DA = new SqlDataAdapter($"INSERT INTO [dbo].[Hotline] ([name]  ,[date_work] ,[date_start]  ,[date_end] ,[time_start] ,[time_end]) VALUES ('{name.Text}' ,'{mDate_work.Text}' ,'{date_start.Text}' ,'{date_end.Text}' ,'{mTime_start.Text}' ,'{mTime_end.Text}')", connect);
                    DA.SelectCommand.ExecuteReader();
                    MessageBox.Show("Hotline info has been added successfully");
                }
                catch (Exception)
                {
                    MessageBox.Show("Error, invalid data");
                }
                finally
                {
                    connect.Close();
                }
            }
            else
            {
                MessageBox.Show("Error,some fields are empty, or contain invalid value");
            }
        }

        private void btnUpdateHotlineInfo_Click(object sender, EventArgs e)
        {
            if (cmbHotline.SelectedItem != null)
            {
                Hotline hotline = (Hotline)cmbHotline.SelectedItem;
                List<string> data = new List<string>();
                data.Add(hotline.Name);
                data.Add(ConvertDatе(hotline.DateWork.ToCharArray(0, 10)));
                data.Add(hotline.DateStart);
                data.Add(hotline.DateEnd);
                data.Add(hotline.TimeStart);
                data.Add(hotline.TimeEnd);
                List<string> newData = new List<string>();
                newData.Add(name.Text);
                newData.Add(mDate_work.Text);
                newData.Add(date_start.Text);
                newData.Add(date_end.Text);
                newData.Add(mTime_start.Text);
                newData.Add(mTime_end.Text);
                for (int i = 0; i < data.Count; i++)
                {
                    if (newData[i] != "")
                    {
                        data[i] = newData[i];
                    }
                }
                if (!char.IsNumber(data[1], 0))
                {
                    data[1] = ConvertDatе(hotline.DateWork.ToCharArray(0, 10));
                }
                if (!char.IsNumber(data[4], 0))
                {
                    data[4] = hotline.TimeStart;
                }
                if (!char.IsNumber(data[5], 0))
                {
                    data[5] = hotline.TimeEnd;
                }

                if (CheckInfo(data[0], data[1], data[2], data[3], data[4], data[5]))
                {
                    SqlConnection connect = new SqlConnection(sqlConnect);
                    try
                    {
                        connect.Open();
                        SqlDataAdapter DA = new SqlDataAdapter($"UPDATE [dbo].[Hotline] SET [name] = '{data[0]}',[date_work] = '{data[1]}',[date_start] = '{data[2]}',[date_end] = '{data[3]}',[time_start] = '{data[4]}',[time_end] = '{data[5]}' WHERE ID={hotline.ID}", connect);
                        DA.SelectCommand.ExecuteReader();
                        MessageBox.Show("Hotline info has been updated successfully");

                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error, invalid data");
                    }
                    finally
                    {
                        connect.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Error,some fields contain invalid value");
                }
            }
            else
            {
                MessageBox.Show("Error,you didnt choose a value from combobox.Please do so");
            }
        }
        private string ConvertDatе(char[] date)
        {
            char[] newDate = new char[date.Length - 2];
            int j = 0;
            for (int i = 6; i < 10; i++)
            {
                newDate[j] = date[i];
                j++;
            }
            for (int i = 3; i < 5; i++)
            {
                newDate[j] = date[i];
                j++;

            }
            for (int i = 0; i < 2; i++)
            {
                newDate[j] = date[i];
                j++;
            }
            string result = new string(newDate);
            return result;
        }
        private bool CheckInfo(string name, string dateWork, string dateStart, string dateEnd, string timeStart, string timeEnd)
        {
            if (name == "" || dateWork == "" || dateStart == "" || dateEnd == "" || timeStart == "" || timeEnd == "")
            {
                return false;
            }
            for (int i = 0; i < dateStart.Length; i++)
            {
                if (char.IsNumber(dateStart, i))
                {
                    return false;
                }
            }
            for (int i = 0; i < dateEnd.Length; i++)
            {
                if (char.IsNumber(dateEnd, i))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
