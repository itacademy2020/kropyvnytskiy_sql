﻿
namespace lab5
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOperantor = new System.Windows.Forms.Button();
            this.btnClient = new System.Windows.Forms.Button();
            this.btnOH = new System.Windows.Forms.Button();
            this.btnHotline = new System.Windows.Forms.Button();
            this.btnSelect1 = new System.Windows.Forms.Button();
            this.btnSelect2 = new System.Windows.Forms.Button();
            this.btnSelect3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOperantor
            // 
            this.btnOperantor.Location = new System.Drawing.Point(12, 12);
            this.btnOperantor.Name = "btnOperantor";
            this.btnOperantor.Size = new System.Drawing.Size(75, 23);
            this.btnOperantor.TabIndex = 0;
            this.btnOperantor.Text = "Operator";
            this.btnOperantor.UseVisualStyleBackColor = true;
            this.btnOperantor.Click += new System.EventHandler(this.btnOperators_Click);
            // 
            // btnClient
            // 
            this.btnClient.Location = new System.Drawing.Point(12, 41);
            this.btnClient.Name = "btnClient";
            this.btnClient.Size = new System.Drawing.Size(75, 23);
            this.btnClient.TabIndex = 1;
            this.btnClient.Text = "Client";
            this.btnClient.UseVisualStyleBackColor = true;
            this.btnClient.Click += new System.EventHandler(this.btnClient_Click);
            // 
            // btnOH
            // 
            this.btnOH.Location = new System.Drawing.Point(12, 99);
            this.btnOH.Name = "btnOH";
            this.btnOH.Size = new System.Drawing.Size(124, 23);
            this.btnOH.TabIndex = 2;
            this.btnOH.Text = "Operator-Hotline";
            this.btnOH.UseVisualStyleBackColor = true;
            this.btnOH.Click += new System.EventHandler(this.btnOH_Click);
            // 
            // btnHotline
            // 
            this.btnHotline.Location = new System.Drawing.Point(12, 70);
            this.btnHotline.Name = "btnHotline";
            this.btnHotline.Size = new System.Drawing.Size(75, 23);
            this.btnHotline.TabIndex = 3;
            this.btnHotline.Text = "Hotline";
            this.btnHotline.UseVisualStyleBackColor = true;
            this.btnHotline.Click += new System.EventHandler(this.btnCall_Click);
            // 
            // btnSelect1
            // 
            this.btnSelect1.Location = new System.Drawing.Point(12, 139);
            this.btnSelect1.Name = "btnSelect1";
            this.btnSelect1.Size = new System.Drawing.Size(75, 23);
            this.btnSelect1.TabIndex = 4;
            this.btnSelect1.Text = "Select 1";
            this.btnSelect1.UseVisualStyleBackColor = true;
            this.btnSelect1.Click += new System.EventHandler(this.btnSelect1_Click);
            // 
            // btnSelect2
            // 
            this.btnSelect2.Location = new System.Drawing.Point(12, 168);
            this.btnSelect2.Name = "btnSelect2";
            this.btnSelect2.Size = new System.Drawing.Size(75, 23);
            this.btnSelect2.TabIndex = 5;
            this.btnSelect2.Text = "Select 2";
            this.btnSelect2.UseVisualStyleBackColor = true;
            this.btnSelect2.Click += new System.EventHandler(this.btnSelect2_Click);
            // 
            // btnSelect3
            // 
            this.btnSelect3.Location = new System.Drawing.Point(12, 197);
            this.btnSelect3.Name = "btnSelect3";
            this.btnSelect3.Size = new System.Drawing.Size(75, 23);
            this.btnSelect3.TabIndex = 6;
            this.btnSelect3.Text = "Select 3";
            this.btnSelect3.UseVisualStyleBackColor = true;
            this.btnSelect3.Click += new System.EventHandler(this.btnSelect3_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(149, 241);
            this.Controls.Add(this.btnSelect3);
            this.Controls.Add(this.btnSelect2);
            this.Controls.Add(this.btnSelect1);
            this.Controls.Add(this.btnHotline);
            this.Controls.Add(this.btnOH);
            this.Controls.Add(this.btnClient);
            this.Controls.Add(this.btnOperantor);
            this.Name = "MainForm";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnOperantor;
        private System.Windows.Forms.Button btnClient;
        private System.Windows.Forms.Button btnOH;
        private System.Windows.Forms.Button btnHotline;
        private System.Windows.Forms.Button btnSelect1;
        private System.Windows.Forms.Button btnSelect2;
        private System.Windows.Forms.Button btnSelect3;
    }
}