﻿
namespace lab5
{
    partial class ClientForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnUpdateClientInfo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.PIB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSetClientInfo = new System.Windows.Forms.Button();
            this.btnGetWorkersInfo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbClients = new System.Windows.Forms.ComboBox();
            this.dgvInfo = new System.Windows.Forms.DataGridView();
            this.mContacts = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.mContacts);
            this.splitContainer1.Panel1.Controls.Add(this.btnUpdateClientInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.PIB);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.btnSetClientInfo);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetWorkersInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbClients);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvInfo);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // btnUpdateClientInfo
            // 
            this.btnUpdateClientInfo.Location = new System.Drawing.Point(121, 264);
            this.btnUpdateClientInfo.Name = "btnUpdateClientInfo";
            this.btnUpdateClientInfo.Size = new System.Drawing.Size(109, 23);
            this.btnUpdateClientInfo.TabIndex = 22;
            this.btnUpdateClientInfo.Text = "Update info";
            this.btnUpdateClientInfo.UseVisualStyleBackColor = true;
            this.btnUpdateClientInfo.Click += new System.EventHandler(this.btnUpdateClientInfo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 216);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "contacts";
            // 
            // PIB
            // 
            this.PIB.Location = new System.Drawing.Point(40, 181);
            this.PIB.Name = "PIB";
            this.PIB.Size = new System.Drawing.Size(190, 22);
            this.PIB.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(40, 161);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "PIB";
            // 
            // btnSetClientInfo
            // 
            this.btnSetClientInfo.Location = new System.Drawing.Point(40, 264);
            this.btnSetClientInfo.Name = "btnSetClientInfo";
            this.btnSetClientInfo.Size = new System.Drawing.Size(75, 23);
            this.btnSetClientInfo.TabIndex = 14;
            this.btnSetClientInfo.Text = "Set info";
            this.btnSetClientInfo.UseVisualStyleBackColor = true;
            this.btnSetClientInfo.Click += new System.EventHandler(this.btnSetClientInfo_Click);
            // 
            // btnGetWorkersInfo
            // 
            this.btnGetWorkersInfo.Location = new System.Drawing.Point(40, 101);
            this.btnGetWorkersInfo.Name = "btnGetWorkersInfo";
            this.btnGetWorkersInfo.Size = new System.Drawing.Size(75, 23);
            this.btnGetWorkersInfo.TabIndex = 10;
            this.btnGetWorkersInfo.Text = "Get info";
            this.btnGetWorkersInfo.UseVisualStyleBackColor = true;
            this.btnGetWorkersInfo.Click += new System.EventHandler(this.btnGetWorkersInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Clients";
            // 
            // cmbClients
            // 
            this.cmbClients.FormattingEnabled = true;
            this.cmbClients.Location = new System.Drawing.Point(40, 62);
            this.cmbClients.Name = "cmbClients";
            this.cmbClients.Size = new System.Drawing.Size(144, 24);
            this.cmbClients.TabIndex = 12;
            // 
            // dgvInfo
            // 
            this.dgvInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInfo.Location = new System.Drawing.Point(3, 0);
            this.dgvInfo.Name = "dgvInfo";
            this.dgvInfo.RowHeadersWidth = 51;
            this.dgvInfo.RowTemplate.Height = 24;
            this.dgvInfo.Size = new System.Drawing.Size(527, 450);
            this.dgvInfo.TabIndex = 1;
            // 
            // mContacts
            // 
            this.mContacts.Location = new System.Drawing.Point(40, 236);
            this.mContacts.Mask = "+380000000000";
            this.mContacts.Name = "mContacts";
            this.mContacts.Size = new System.Drawing.Size(190, 22);
            this.mContacts.TabIndex = 23;
            // 
            // ClientForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "ClientForm";
            this.Text = "ClientForm";
            this.Load += new System.EventHandler(this.ClientForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvInfo;
        private System.Windows.Forms.Button btnUpdateClientInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox PIB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSetClientInfo;
        private System.Windows.Forms.Button btnGetWorkersInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbClients;
        private System.Windows.Forms.MaskedTextBox mContacts;
    }
}