﻿
namespace lab5
{
    partial class OHForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbOH = new System.Windows.Forms.ComboBox();
            this.cmbHotline = new System.Windows.Forms.ComboBox();
            this.cmbOperator = new System.Windows.Forms.ComboBox();
            this.btnDeleteOHInfo = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSetOHInfo = new System.Windows.Forms.Button();
            this.btnGetOHInfo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.dgvInfo = new System.Windows.Forms.DataGridView();
            this.cmbOperator1 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cmbOperator1);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.cmbOH);
            this.splitContainer1.Panel1.Controls.Add(this.cmbHotline);
            this.splitContainer1.Panel1.Controls.Add(this.cmbOperator);
            this.splitContainer1.Panel1.Controls.Add(this.btnDeleteOHInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.btnSetOHInfo);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetOHInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvInfo);
            this.splitContainer1.Size = new System.Drawing.Size(800, 450);
            this.splitContainer1.SplitterDistance = 266;
            this.splitContainer1.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 239);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(131, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "Operator-Hotline ID";
            // 
            // cmbOH
            // 
            this.cmbOH.FormattingEnabled = true;
            this.cmbOH.Location = new System.Drawing.Point(15, 259);
            this.cmbOH.Name = "cmbOH";
            this.cmbOH.Size = new System.Drawing.Size(82, 24);
            this.cmbOH.TabIndex = 25;
            // 
            // cmbHotline
            // 
            this.cmbHotline.FormattingEnabled = true;
            this.cmbHotline.Location = new System.Drawing.Point(154, 128);
            this.cmbHotline.Name = "cmbHotline";
            this.cmbHotline.Size = new System.Drawing.Size(108, 24);
            this.cmbHotline.TabIndex = 24;
            // 
            // cmbOperator
            // 
            this.cmbOperator.FormattingEnabled = true;
            this.cmbOperator.Location = new System.Drawing.Point(11, 128);
            this.cmbOperator.Name = "cmbOperator";
            this.cmbOperator.Size = new System.Drawing.Size(132, 24);
            this.cmbOperator.TabIndex = 23;
            // 
            // btnDeleteOHInfo
            // 
            this.btnDeleteOHInfo.Location = new System.Drawing.Point(11, 289);
            this.btnDeleteOHInfo.Name = "btnDeleteOHInfo";
            this.btnDeleteOHInfo.Size = new System.Drawing.Size(109, 23);
            this.btnDeleteOHInfo.TabIndex = 22;
            this.btnDeleteOHInfo.Text = "Delete info";
            this.btnDeleteOHInfo.UseVisualStyleBackColor = true;
            this.btnDeleteOHInfo.Click += new System.EventHandler(this.btnDeleteOHInfo_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(151, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Hotline name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Operator PIB";
            // 
            // btnSetOHInfo
            // 
            this.btnSetOHInfo.Location = new System.Drawing.Point(12, 175);
            this.btnSetOHInfo.Name = "btnSetOHInfo";
            this.btnSetOHInfo.Size = new System.Drawing.Size(75, 23);
            this.btnSetOHInfo.TabIndex = 14;
            this.btnSetOHInfo.Text = "Set info";
            this.btnSetOHInfo.UseVisualStyleBackColor = true;
            this.btnSetOHInfo.Click += new System.EventHandler(this.btnSetOHInfo_Click);
            // 
            // btnGetOHInfo
            // 
            this.btnGetOHInfo.Location = new System.Drawing.Point(10, 68);
            this.btnGetOHInfo.Name = "btnGetOHInfo";
            this.btnGetOHInfo.Size = new System.Drawing.Size(75, 23);
            this.btnGetOHInfo.TabIndex = 10;
            this.btnGetOHInfo.Text = "Get info";
            this.btnGetOHInfo.UseVisualStyleBackColor = true;
            this.btnGetOHInfo.Click += new System.EventHandler(this.btnGetWorkersInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Operator-Hotline";
            // 
            // dgvInfo
            // 
            this.dgvInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInfo.Location = new System.Drawing.Point(-2, 0);
            this.dgvInfo.Name = "dgvInfo";
            this.dgvInfo.RowHeadersWidth = 51;
            this.dgvInfo.RowTemplate.Height = 24;
            this.dgvInfo.Size = new System.Drawing.Size(537, 450);
            this.dgvInfo.TabIndex = 1;
            // 
            // cmbOperator1
            // 
            this.cmbOperator1.FormattingEnabled = true;
            this.cmbOperator1.Location = new System.Drawing.Point(12, 38);
            this.cmbOperator1.Name = "cmbOperator1";
            this.cmbOperator1.Size = new System.Drawing.Size(132, 24);
            this.cmbOperator1.TabIndex = 27;
            // 
            // OHForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "OHForm";
            this.Text = "OHForm";
            this.Load += new System.EventHandler(this.OHForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgvInfo;
        private System.Windows.Forms.Button btnDeleteOHInfo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSetOHInfo;
        private System.Windows.Forms.Button btnGetOHInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbOH;
        private System.Windows.Forms.ComboBox cmbHotline;
        private System.Windows.Forms.ComboBox cmbOperator;
        private System.Windows.Forms.ComboBox cmbOperator1;
    }
}