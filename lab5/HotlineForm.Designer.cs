﻿
namespace lab5
{
    partial class HotlineForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label7 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.date_end = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.date_start = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnUpdateHotlineInfo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSetHotlineInfo = new System.Windows.Forms.Button();
            this.btnGetWorkersInfo = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbHotline = new System.Windows.Forms.ComboBox();
            this.dgvInfo = new System.Windows.Forms.DataGridView();
            this.mDate_work = new System.Windows.Forms.MaskedTextBox();
            this.mTime_start = new System.Windows.Forms.MaskedTextBox();
            this.mTime_end = new System.Windows.Forms.MaskedTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.mTime_start);
            this.splitContainer1.Panel1.Controls.Add(this.mTime_end);
            this.splitContainer1.Panel1.Controls.Add(this.mDate_work);
            this.splitContainer1.Panel1.Controls.Add(this.label7);
            this.splitContainer1.Panel1.Controls.Add(this.name);
            this.splitContainer1.Panel1.Controls.Add(this.label6);
            this.splitContainer1.Panel1.Controls.Add(this.label5);
            this.splitContainer1.Panel1.Controls.Add(this.date_end);
            this.splitContainer1.Panel1.Controls.Add(this.label4);
            this.splitContainer1.Panel1.Controls.Add(this.date_start);
            this.splitContainer1.Panel1.Controls.Add(this.label3);
            this.splitContainer1.Panel1.Controls.Add(this.btnUpdateHotlineInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label2);
            this.splitContainer1.Panel1.Controls.Add(this.btnSetHotlineInfo);
            this.splitContainer1.Panel1.Controls.Add(this.btnGetWorkersInfo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.cmbHotline);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.dgvInfo);
            this.splitContainer1.Size = new System.Drawing.Size(1234, 450);
            this.splitContainer1.SplitterDistance = 263;
            this.splitContainer1.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 320);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 17);
            this.label7.TabIndex = 32;
            this.label7.Text = "Time end";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(23, 100);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(190, 22);
            this.name.TabIndex = 29;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 80);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 17);
            this.label6.TabIndex = 30;
            this.label6.Text = "Name";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 277);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 17);
            this.label5.TabIndex = 28;
            this.label5.Text = "Time start";
            // 
            // date_end
            // 
            this.date_end.Location = new System.Drawing.Point(23, 250);
            this.date_end.Name = "date_end";
            this.date_end.Size = new System.Drawing.Size(190, 22);
            this.date_end.TabIndex = 25;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 230);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 26;
            this.label4.Text = "Date end";
            // 
            // date_start
            // 
            this.date_start.Location = new System.Drawing.Point(23, 198);
            this.date_start.Name = "date_start";
            this.date_start.Size = new System.Drawing.Size(190, 22);
            this.date_start.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 178);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 17);
            this.label3.TabIndex = 24;
            this.label3.Text = "Date start";
            // 
            // btnUpdateHotlineInfo
            // 
            this.btnUpdateHotlineInfo.Location = new System.Drawing.Point(104, 378);
            this.btnUpdateHotlineInfo.Name = "btnUpdateHotlineInfo";
            this.btnUpdateHotlineInfo.Size = new System.Drawing.Size(109, 23);
            this.btnUpdateHotlineInfo.TabIndex = 22;
            this.btnUpdateHotlineInfo.Text = "Update info";
            this.btnUpdateHotlineInfo.UseVisualStyleBackColor = true;
            this.btnUpdateHotlineInfo.Click += new System.EventHandler(this.btnUpdateHotlineInfo_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Date work";
            // 
            // btnSetHotlineInfo
            // 
            this.btnSetHotlineInfo.Location = new System.Drawing.Point(23, 378);
            this.btnSetHotlineInfo.Name = "btnSetHotlineInfo";
            this.btnSetHotlineInfo.Size = new System.Drawing.Size(75, 23);
            this.btnSetHotlineInfo.TabIndex = 14;
            this.btnSetHotlineInfo.Text = "Set info";
            this.btnSetHotlineInfo.UseVisualStyleBackColor = true;
            this.btnSetHotlineInfo.Click += new System.EventHandler(this.btnSetWorkerInfo_Click);
            // 
            // btnGetWorkersInfo
            // 
            this.btnGetWorkersInfo.Location = new System.Drawing.Point(173, 43);
            this.btnGetWorkersInfo.Name = "btnGetWorkersInfo";
            this.btnGetWorkersInfo.Size = new System.Drawing.Size(75, 23);
            this.btnGetWorkersInfo.TabIndex = 10;
            this.btnGetWorkersInfo.Text = "Get info";
            this.btnGetWorkersInfo.UseVisualStyleBackColor = true;
            this.btnGetWorkersInfo.Click += new System.EventHandler(this.btnGetWorkersInfo_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 17);
            this.label1.TabIndex = 11;
            this.label1.Text = "Hotline";
            // 
            // cmbHotline
            // 
            this.cmbHotline.FormattingEnabled = true;
            this.cmbHotline.Location = new System.Drawing.Point(23, 42);
            this.cmbHotline.Name = "cmbHotline";
            this.cmbHotline.Size = new System.Drawing.Size(144, 24);
            this.cmbHotline.TabIndex = 12;
            // 
            // dgvInfo
            // 
            this.dgvInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInfo.Location = new System.Drawing.Point(3, 0);
            this.dgvInfo.Name = "dgvInfo";
            this.dgvInfo.RowHeadersWidth = 51;
            this.dgvInfo.RowTemplate.Height = 24;
            this.dgvInfo.Size = new System.Drawing.Size(964, 450);
            this.dgvInfo.TabIndex = 1;
            // 
            // mDate_work
            // 
            this.mDate_work.AccessibleDescription = "";
            this.mDate_work.AccessibleName = "";
            this.mDate_work.Location = new System.Drawing.Point(23, 153);
            this.mDate_work.Mask = "0000-00-00";
            this.mDate_work.Name = "mDate_work";
            this.mDate_work.Size = new System.Drawing.Size(190, 22);
            this.mDate_work.TabIndex = 33;
            // 
            // mTime_start
            // 
            this.mTime_start.AccessibleDescription = "";
            this.mTime_start.AccessibleName = "";
            this.mTime_start.Location = new System.Drawing.Point(23, 297);
            this.mTime_start.Mask = "00:00:00";
            this.mTime_start.Name = "mTime_start";
            this.mTime_start.Size = new System.Drawing.Size(190, 22);
            this.mTime_start.TabIndex = 34;
            // 
            // mTime_end
            // 
            this.mTime_end.AccessibleDescription = "";
            this.mTime_end.AccessibleName = "";
            this.mTime_end.Location = new System.Drawing.Point(23, 340);
            this.mTime_end.Mask = "00:00:00";
            this.mTime_end.Name = "mTime_end";
            this.mTime_end.Size = new System.Drawing.Size(190, 22);
            this.mTime_end.TabIndex = 35;
            // 
            // HotlineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 450);
            this.Controls.Add(this.splitContainer1);
            this.Name = "HotlineForm";
            this.Text = "HotlineForm";
            this.Load += new System.EventHandler(this.CallForm_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnUpdateHotlineInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSetHotlineInfo;
        private System.Windows.Forms.Button btnGetWorkersInfo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbHotline;
        private System.Windows.Forms.DataGridView dgvInfo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox date_end;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox date_start;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.MaskedTextBox mDate_work;
        private System.Windows.Forms.MaskedTextBox mTime_start;
        private System.Windows.Forms.MaskedTextBox mTime_end;
    }
}