﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab5
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnOperators_Click(object sender, EventArgs e)
        {
            OperatorForm oper = new OperatorForm();
            oper.ShowDialog();
        }

        private void btnClient_Click(object sender, EventArgs e)
        {
            ClientForm client = new ClientForm();
            client.ShowDialog();
        }

        private void btnOH_Click(object sender, EventArgs e)
        {
            OHForm OH = new OHForm();
            OH.ShowDialog();
        }

        private void btnCall_Click(object sender, EventArgs e)
        {
            HotlineForm call = new HotlineForm();
            call.ShowDialog();
        }

        private void btnSelect1_Click(object sender, EventArgs e)
        {
            Select1Form select1= new Select1Form();
            select1.ShowDialog();
        }

        private void btnSelect2_Click(object sender, EventArgs e)
        {
            Select2Form select2 = new Select2Form();
            select2.ShowDialog();
        }

        private void btnSelect3_Click(object sender, EventArgs e)
        {
            Select3Form select3 = new Select3Form();
            select3.ShowDialog();
        }
    }
}
