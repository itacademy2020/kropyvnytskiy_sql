﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace lab5
{
    public partial class Select1Form : Form
    {
        private readonly string sqlConnect = "Server=tcp:itacademy.database.windows.net,1433;Database=Kropyvnytskyi;User ID=Kropyvnytskyi;Password=Wboe73015;Trusted_Connection=False;Encrypt=True;";
        public Select1Form()
        {
            InitializeComponent();
        }

        private void Select1Form_Load(object sender, EventArgs e)
        {
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT Operator.ID,Operator.PIB FROM [dbo].[Operator]", connect);
            SqlDataReader dataReader = DA.SelectCommand.ExecuteReader();
            cmbWorkers.Items.Clear();
            while (dataReader.Read())
            {
                Work oper = new Work();
                oper.ID = (int)dataReader[0];
                oper.Name = dataReader[1].ToString();
                cmbWorkers.Items.Add(oper);
                cmbWorkers.DisplayMember = "Name";
            }
            connect.Close();
        }

        private void btnGetInfo_Click(object sender, EventArgs e)
        {
            Work oper = (Work)cmbWorkers.SelectedItem;
            if (cmbWorkers.SelectedItem == null)
            {
                MessageBox.Show("You didnt choose anything, so the system will show you first value");
                oper = (Work)cmbWorkers.Items[0];
            }
            string firstDate=mFirst_date.Text, lastDate=mLast_date.Text;

            if (char.IsNumber(firstDate, 0) && char.IsNumber(lastDate, 0))
            {
                SqlConnection connect = new SqlConnection(sqlConnect);
                try
                {
                    connect.Open();
                    SqlDataAdapter DA = new SqlDataAdapter($"SELECT Hotline.ID, Call.problem_type, Call.date_call FROM [dbo].[Call] LEFT JOIN [dbo].[Hotline] ON Call.hotline_ID = Hotline.ID LEFT JOIN[dbo].[Operator] ON Call.operator_ID = Operator.ID WHERE date_call > '{firstDate}' AND date_call < '{lastDate}' AND Operator.ID = '{oper.ID}'", connect);
                    DataSet DS = new DataSet();
                    DA.Fill(DS, "select1_info");
                    dgvInfo.DataSource = DS.Tables["select1_info"];
                    dgvInfo.Refresh();
                  
                }
                catch(Exception)
                {
                    MessageBox.Show("Error, Invalid data. ");
                }
                finally
                {
                    connect.Close();
                }
            }
            else
            {
                MessageBox.Show("Error, some date fields are empty");
            }
        }
    }
}
