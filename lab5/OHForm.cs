﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace lab5
{
    public partial class OHForm : Form
    {
        private readonly string sqlConnect = "Server=tcp:itacademy.database.windows.net,1433;Database=Kropyvnytskyi;User ID=Kropyvnytskyi;Password=Wboe73015;Trusted_Connection=False;Encrypt=True;";
        public OHForm()
        {
            InitializeComponent();
        }

        private void OHForm_Load(object sender, EventArgs e)
        {
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT ID,hotline_ID,operator_ID FROM [dbo].[Operator-Hotline]", connect);
            SqlDataReader dataReader = DA.SelectCommand.ExecuteReader();
            cmbOH.Items.Clear();
            while (dataReader.Read())
            {
                Operator_Hotline OH = new Operator_Hotline();
                OH.ID = (int)dataReader[0];
                OH.operatorID = (int)dataReader[1];
                OH.HotlineID = (int)dataReader[2];
                cmbOH.Items.Add(OH);
                cmbOH.DisplayMember = "ID";
            }
            dataReader.Close();

            DA.SelectCommand = new SqlCommand("SELECT ID,PIB FROM [dbo].[Operator]", connect);
            dataReader=DA.SelectCommand.ExecuteReader();
            cmbOperator.Items.Clear();
            while (dataReader.Read())
            {
                Work oper = new Work();
                oper.ID = (int)dataReader[0];
                oper.Name = dataReader[1].ToString();
                cmbOperator.Items.Add(oper);
                cmbOperator.DisplayMember = "Name";
            }
            dataReader.Close();

            DA.SelectCommand = new SqlCommand("SELECT ID,PIB FROM [dbo].[Operator]", connect);
            dataReader = DA.SelectCommand.ExecuteReader();
            cmbOperator1.Items.Clear();
            while (dataReader.Read())
            {
                Work oper = new Work();
                oper.ID = (int)dataReader[0];
                oper.Name = dataReader[1].ToString();
                cmbOperator1.Items.Add(oper);
                cmbOperator1.DisplayMember = "Name";
            }
            dataReader.Close();

            DA.SelectCommand = new SqlCommand("SELECT ID, name FROM [dbo].[Hotline]", connect);
            dataReader = DA.SelectCommand.ExecuteReader();
            cmbHotline.Items.Clear();
            while (dataReader.Read())
            {
                Work hotline = new Work();
                hotline.ID = (int)dataReader[0];
                hotline.Name = dataReader[1].ToString();
                cmbHotline.Items.Add(hotline);
                cmbHotline.DisplayMember = "Name";
            }

        }
        private void btnGetWorkersInfo_Click(object sender, EventArgs e)
        {
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA=new SqlDataAdapter();
            if (cmbOperator1.SelectedItem != null)
            {
                Work oper = (Work)cmbOperator1.SelectedItem;
                 DA.SelectCommand = new SqlCommand($"SELECT * FROM [dbo].[Operator-Hotline] WHERE operator_ID={oper.ID}", connect);
            }
            else
            {
                DA.SelectCommand = new SqlCommand("SELECT * FROM [dbo].[Operator-Hotline]", connect);
            }
            DataSet DS = new DataSet();
            DA.Fill(DS, "OH_info");
            dgvInfo.DataSource = DS.Tables["OH_info"];
            dgvInfo.Refresh();
            connect.Close();
        }

        private void btnSetOHInfo_Click(object sender, EventArgs e)
        {
            if(cmbOperator.SelectedItem!=null && cmbHotline.SelectedItem != null)
            {
            Work oper = (Work)cmbOperator.SelectedItem;
            Work hotline = (Work)cmbHotline.SelectedItem;
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter($"INSERT INTO[dbo].[Operator-Hotline] ([operator_ID],[hotline_ID]) VALUES({oper.ID}, {hotline.ID})", connect);
            DA.SelectCommand.ExecuteReader();
            MessageBox.Show("Info has been added successfully");
            connect.Close();
            }
            else
            {
                MessageBox.Show("Error,some fields are empty");
            }

        }
        private void btnDeleteOHInfo_Click(object sender, EventArgs e)
        {
            Operator_Hotline OH = (Operator_Hotline)cmbOH.SelectedItem;
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter($"DELETE FROM[dbo].[Operator-Hotline] WHERE ID={OH.ID}", connect);
            DA.SelectCommand.ExecuteReader();
            MessageBox.Show("Info has been deleted successfully");
            connect.Close();
        }
    }
}