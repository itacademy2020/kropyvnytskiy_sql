﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace lab5
{
    public partial class OperatorForm : Form
    {
        private readonly string sqlConnect = "Server=tcp:itacademy.database.windows.net,1433;Database=Kropyvnytskyi;User ID=Kropyvnytskyi;Password=Wboe73015;Trusted_Connection=False;Encrypt=True;";
        public OperatorForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT Operator.ID,Operator.PIB, Operator.contacts, Operator.date_start,Operator.address FROM [dbo].[Operator]", connect);
            SqlDataReader dataReader = DA.SelectCommand.ExecuteReader();
            cmbWorkers.Items.Clear();
            while (dataReader.Read())
            {
                Operator oper = new Operator();
                oper.ID = (int)dataReader[0];
                oper.Name = dataReader[1].ToString();
                oper.Contacts = dataReader[2].ToString();
                oper.DateStart = dataReader[3].ToString();
                oper.Address = dataReader[4].ToString();
                cmbWorkers.Items.Add(oper);
                cmbWorkers.DisplayMember = "Name";
            }
            connect.Close();
        }

        private void btnGetWorkersInfo_Click(object sender, EventArgs e)
        {

            Operator oper = (Operator)cmbWorkers.SelectedItem;
            if (cmbWorkers.SelectedItem == null)
            {
                MessageBox.Show("You didnt choose anything, so the system will show you first value");
                oper = (Operator)cmbWorkers.Items[0];
            }
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT Operator.ID,Operator.PIB,Operator.contacts,Operator.date_start,Operator.address FROM Operator WHERE ID=" + oper.ID.ToString(), connect);
            DataSet DS = new DataSet();
            DA.Fill(DS, "oper_info");
            dgvInfo.DataSource = DS.Tables["oper_info"];
            dgvInfo.Refresh();
            connect.Close();
        }

        private void btnSetWorkerInfo_Click(object sender, EventArgs e)
        {
            if (CheckInfo(PIB.Text, mContacts.Text, mDateStart.Text, address.Text))
            {
                SqlConnection connect = new SqlConnection(sqlConnect);
                try
                {
                   
                    connect.Open();
                    SqlDataAdapter DA = new SqlDataAdapter($"INSERT INTO [dbo].[Operator]([PIB] ,[contacts]  ,[date_start]   ,[address]) VALUES ('{PIB.Text}'  ,'{mContacts.Text}'  ,'{mDateStart.Text}' ,'{address.Text}')", connect);
                    DA.SelectCommand.ExecuteReader();
                    MessageBox.Show("Operator info has been added successfully");
                    
                }
                catch(Exception)
                {
                    MessageBox.Show("Error, invalid data");
                }
                finally
                {
                    connect.Close();
                }

            }
            else
            {
                MessageBox.Show("Error,some fields are empty, or contain invalid value");
            }

        }
        private void btnUpdateInfo_Click(object sender, EventArgs e)
        {
            if (cmbWorkers.SelectedItem != null)
            {
                Operator oper = (Operator)cmbWorkers.SelectedItem;
                List<string> data = new List<string>();
                data.Add(oper.Name);
                data.Add(oper.Contacts);
                data.Add(ConvertDatе(oper.DateStart.ToCharArray(0, 10)));
                data.Add(oper.Address);
                List<string> newData = new List<string>();
                newData.Add(PIB.Text);
                newData.Add(mContacts.Text);
                newData.Add(mDateStart.Text);
                newData.Add(address.Text);
                for (int i = 0; i < data.Count; i++)
                {
                    if (newData[i] != "")
                    {
                        data[i] = newData[i];
                    }
                }
                if (data[1] == "+38")
                {
                    data[1] = oper.Contacts;
                }
                if (data[2] == "    -  -")
                {
                    data[2] = ConvertDatе(oper.DateStart.ToCharArray(0, 10));
                }
                if (CheckInfo(data[0], data[1], data[2], data[3]))
                {
                    SqlConnection connect = new SqlConnection(sqlConnect);
                    try
                    {
                        connect.Open();
                        SqlDataAdapter DA = new SqlDataAdapter($"UPDATE [dbo].[Operator] SET [PIB] = '{data[0]}',[contacts] = '{data[1]}',[date_start] = '{data[2]}',[address] = '{data[3]}' WHERE ID={oper.ID}", connect);
                        DA.SelectCommand.ExecuteReader();
                        MessageBox.Show("Operator info has been updated successfully");
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error, invalid data");
                    }
                    finally
                    {
                        connect.Close();
                    }



                }
                else
                {
                    MessageBox.Show("Error,some fields contain invalid value");
                }
            }
            else
            {
                MessageBox.Show("Error,you didnt choose a value from combobox.Please do so");
            }
        }
        private string ConvertDatе(char[] date)
        {
            char[] newDate = new char[date.Length - 2];
            int j = 0;
            for (int i = 6; i < 10; i++)
            {
                newDate[j] = date[i];
                j++;
            }
            for (int i = 3; i < 5; i++)
            {
                newDate[j] = date[i];
                j++;

            }
            for (int i = 0; i < 2; i++)
            {
                newDate[j] = date[i];
                j++;
            }
            string result = new string(newDate);
            return result;
        }
        private bool CheckInfo(string PIB, string contacts, string dateStart, string address)
        {
            if (PIB == "" || contacts == "" || dateStart == "" || address == "")
            {
                return false;
            }
            for (int i = 0; i < PIB.Length; i++)
            {
                if (char.IsNumber(PIB, i))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
