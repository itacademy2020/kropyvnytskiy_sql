﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace lab5
{
    public partial class Select2Form : Form
    {
        private readonly string sqlConnect = "Server=tcp:itacademy.database.windows.net,1433;Database=Kropyvnytskyi;User ID=Kropyvnytskyi;Password=Wboe73015;Trusted_Connection=False;Encrypt=True;";
        public Select2Form()
        {
            InitializeComponent();
        }

        private void Select2Form_Load(object sender, EventArgs e)
        {
            SqlConnection connect = new SqlConnection(sqlConnect);
            connect.Open();
            SqlDataAdapter DA = new SqlDataAdapter("SELECT Operator.ID,Operator.PIB FROM [dbo].[Operator]", connect);
            SqlDataReader dataReader = DA.SelectCommand.ExecuteReader();
            cmbWorkers.Items.Clear();
            while (dataReader.Read())
            {
                Work oper = new Work();
                oper.ID = (int)dataReader[0];
                oper.Name = dataReader[1].ToString();
                cmbWorkers.Items.Add(oper);
                cmbWorkers.DisplayMember = "Name";
            }
            connect.Close();
        }

        private void btnGetInfo_Click(object sender, EventArgs e)
        {
            if(cmbWorkers.SelectedItem == null) 
            {

            }
            else
            {
                Work oper = (Work)cmbWorkers.SelectedItem;
                SqlConnection connect = new SqlConnection(sqlConnect);
                connect.Open();
                SqlDataAdapter DA = new SqlDataAdapter($" SELECT Hotline.name,COUNT(Call.ID) FROM[dbo].[Call] INNER JOIN[dbo].[Hotline] ON Hotline.ID = Call.Hotline_ID INNER JOIN[dbo].[Operator] ON[Call].[operator_ID] =[Operator].[ID] WHERE Operator.ID = {oper.ID} GROUP BY Hotline.name", connect);
                DataSet DS = new DataSet();
                DA.Fill(DS, "select2_info");
                dgvInfo.DataSource = DS.Tables["select2_info"];
                dgvInfo.Refresh();
                connect.Close();
            }

        }
    }
}
